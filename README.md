## Architecture

![Logo](assets/architecture.png)


## Repositories

- Frontend : https://gitlab.com/muhammadandikakurniawan1/nongki-app/frontend

- Backend : https://gitlab.com/muhammadandikakurniawan1/nongki-app/backend


## Docker compose

```yaml
version: '3'

services:
      
  postgres:
    image: postgres:10.0
    hostname: nongkipostgres
    volumes:
      - ./data/postgres:/var/lib/postgresql/data
    environment:
      - POSTGRES_DB=postgres
      - POSTGRES_USER=user
      - POSTGRES_PASSWORD=password
    networks:
      - nongki_net
    ports:
      - 5434:5432

  minio:
    image: minio/minio
    # command: server /data
    command: server /data --console-address :9001
    hostname: nongkiminio
    environment:
      - MINIO_ACCESS_KEY=admin
      - MINIO_SECRET_KEY=password
    ports:
      - 9000:9000
      - 9001:9001
    volumes:
      - ./data/minio:/data
    networks:
      - nongki_net

  mongo1:
    hostname: mongo1
    image: mongo:4.4
    expose:
      - 27017
    ports:
      - 30001:27017 
    # restart: always
    networks:
      - nongki_net
    command: mongod --replSet nongki-mongo-rs
  mongo2:
    hostname: mongo2
    image: mongo:4.4
    expose:
      - 27017
    ports:
      - 30002:27017
    # restart: always
    networks:
      - nongki_net
    command: mongod --replSet nongki-mongo-rs
  mongo3:
    hostname: mongo3
    image: mongo:4.4
    expose:
      - 27017
    ports:
      - 30003:27017
    # restart: always
    networks:
      - nongki_net
    command: mongod --replSet nongki-mongo-rs

  envoy:
    networks:
      - nongki_net
    ports:
      - "9901:9901"
      - "4433:4433"
      - "4423:4423"

      - "4434:4434"
      - "4424:4424"

      - "4425:4425"
      - "4435:4435"
      - "4445:4445"
    build: 
      context: ./envoy
      dockerfile: Dockerfile
    image: nongki/envoy
    volumes:
      - ./envoy/data/envoy/envoy.yaml:/etc/envoy/envoy.yaml

  zookeeper:
    image: wurstmeister/zookeeper
    container_name: zookeeper
    ports:
      - "2181:2181"
    networks:
      - nongki_net
  
  kafka:
    image: wurstmeister/kafka
    container_name: kafka
    ports:
      - "9092:9092"
    environment:
      KAFKA_ADVERTISED_HOST_NAME: localhost
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
    networks:
      - nongki_net

# ==================================================== SERVICES ====================================================

  nongkiapp-be-userservice:
    hostname: nongkiapp-be-userservice
    environment:
      - HTTP_PORT=2423
      - GRPC_PORT=2433
      - APP_KEY=user-service-key
      - PRIVATE_APP_KEY=user-service-private-key
      - DB_POSTGRESQL_HOST=nongkipostgres
      - DB_POSTGRESQL_PORT=5432
      - DB_POSTGRESQL_USER=user
      - DB_POSTGRESQL_PASSWORD=password
      - DB_POSTGRESQL_DBNAME=user_service
      - DB_POSTGRESQL_PARAMETER=sslmode=disable TimeZone=Asia/Jakarta
      - GOOGLE_SERVICE_GET_USERINFO_ENDPOINT=https://www.googleapis.com/oauth2/v1/userinfo
      - CRYPTO_AES_KEY=AESKEYXYZMULTIFINANCE12023030923
      - CRYPTO_AES_IV=acfa7a047800b2f2
      - JWT_SECRETKEY=awanservicejwtsecret2042001
    ports:
      - "24231:2423"
      - "24331:2433"
    networks:
      - nongki_net
    build: 
      context: ../be/user_service
      dockerfile: Dockerfile

  nongkiapp-be-eventmanagementservice:
    hostname: nongkiapp-be-eventmanagementservice
    environment:
      - HTTP_PORT=2424
      - GRPC_PORT=2434
      - APP_KEY=event-management-service
      - PRIVATE_APP_KEY=event-management-service-private-app-key
      - DB_POSTGRESQL_HOST=nongkipostgres
      - DB_POSTGRESQL_PORT=5432
      - DB_POSTGRESQL_USER=user
      - DB_POSTGRESQL_PASSWORD=password
      - DB_POSTGRESQL_DBNAME=event_management_service
      - DB_POSTGRESQL_PARAMETER=sslmode=disable TimeZone=Asia/Jakarta
      - GOOGLE_SERVICE_GET_USERINFO_ENDPOINT=https://www.googleapis.com/oauth2/v1/userinfo
      - CRYPTO_AES_KEY=AESKEYXYZMULTIFINANCE12023030923
      - CRYPTO_AES_IV=acfa7a047800b2f2
      - JWT_SECRETKEY=awanservicejwtsecret2042001
      - USER_SERVICE_GRPC_HOST=nongkiapp-be-userservice:2433
      - USER_SERVICE_APP_PRIVATE_KEY=user-service-private-key
      - OBJECT_STORAGE_TIMEOUT=5000000000
      - OBJECT_STORAGE_HOST=nongkiminio:9000
      - OBJECT_STORAGE_KEY_ID=admin
      - OBJECT_STORAGE_ACESS_KEY=password
      - OBJECT_STORAGE_SSL=false
      - OBJECT_STORAGE_BUCKET=ideas-user-service
      - OBJECT_STORAGE_SUB_BUCKET_USER=user
      - OBJECT_STORAGE_SUB_BUCKET_USER_ACCOUNT=user-accounts
      - OBJECT_STORAGE_SUB_BUCKET_ASSET=assets
      - OBJECT_STORAGE_EXTERNAL_HOST=http://127.0.0.1:9000
      - EVENT_CHAT_GROUP_SERVICE_HTTP_HOST=http://nongkiapp-be-eventchatgroupservice:2425
      - EVENT_CHAT_GROUP_SERVICE_APP_PRIVATE_KEY=event-chat-group-service-private-app-key
      - EVENT_CHAT_GROUP_SERVICE_APP_KEY=event-chat-group-service-app-key
    ports:
      - "24241:2424"
      - "24341:2434"
    networks:
      - nongki_net
    build: 
      context: ../be/event_management_service
      dockerfile: Dockerfile

  nongkiapp-be-eventchatgroupservice:
    hostname: nongkiapp-be-eventchatgroupservice
    environment:
      - HTTP_PORT=2425
      - GRPC_PORT=2435
      - WEBSOCKET_PORT=2445
      - APP_KEY=event-chat-group-service-app-key
      - PRIVATE_APP_KEY=event-chat-group-service-private-app-key
      - DB_POSTGRESQL_HOST=nongkipostgres
      - DB_POSTGRESQL_PORT=5432
      - DB_POSTGRESQL_USER=user
      - DB_POSTGRESQL_PASSWORD=password
      - DB_POSTGRESQL_DBNAME=event_chat_group_service
      - DB_POSTGRESQL_PARAMETER=sslmode=disable TimeZone=Asia/Jakarta
      - GOOGLE_SERVICE_GET_USERINFO_ENDPOINT=https://www.googleapis.com/oauth2/v1/userinfo
      - CRYPTO_AES_KEY=AESKEYXYZMULTIFINANCE12023030923
      - CRYPTO_AES_IV=acfa7a047800b2f2
      - JWT_SECRETKEY=awanservicejwtsecret2042001
      - USER_SERVICE_GRPC_HOST=nongkiapp-be-userservice:2433
      - USER_SERVICE_PRIVATE_APP_KEY=user-service-private-key
      - EVENT_MANAGEMENT_SERVICE_GRPC_HOST=nongkiapp-be-eventmanagementservice:2434
      - EVENT_MANAGEMENT_SERVICE_PRIVATE_APP_KEY=event-management-service-private-app-key
      - OBJECT_STORAGE_TIMEOUT=5000000000
      - OBJECT_STORAGE_HOST=nongkiminio:9000
      - OBJECT_STORAGE_KEY_ID=admin
      - OBJECT_STORAGE_ACESS_KEY=password
      - OBJECT_STORAGE_SSL=false
      - OBJECT_STORAGE_BUCKET=ideas-user-service
      - OBJECT_STORAGE_SUB_BUCKET_USER=user
      - OBJECT_STORAGE_SUB_BUCKET_USER_ACCOUNT=user-accounts
      - OBJECT_STORAGE_SUB_BUCKET_ASSET=assets
      - OBJECT_STORAGE_EXTERNAL_HOST=http://127.0.0.1:9000
      - MONGODB_HOST=mongo1,mongo2,mongo3
      - MONGODB_PORT=27017,27017,27017
      - MONGODB_USER=,,
      - MONGODB_PASSWORD=,,
      - MONGODB_PARAMETER=directConnection=true,directConnection=true,directConnection=true
      - MONGODB_DBNAME=event_chat_group_service,event_chat_group_service,event_chat_group_service
      - MESSAGE_BROKER_CLIENT_TYPE=rabbitmq
      - MESSAGE_BROKER_HOST=nongkirabbitmq
      - MESSAGE_BROKER_PORT=5672
      - MESSAGE_BROKER_USERNAME=guest
      - MESSAGE_BROKER_PASSWORD=guest
      - OBJECT_STORAGE_BUCKET_EVENT_CHAT_GROUP=event-chat-group-files
    ports:
      - "24251:2425"
      - "24351:2435"
      - "24451:2445"
    networks:
      - nongki_net
    build: 
      context: ../be/event_chat_group_service
      dockerfile: Dockerfile

networks:
  nongki_net:
```